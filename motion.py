# pylint: disable=C0103
"""ToDo."""

from datetime import datetime
import json
import config
from device import Device

class Motion(Device):
    """ToDo."""

    def __init__(self):
        Device.__init__(self)
        self._motion = None

    @property
    def motion(self):
        """ToDo."""
        return self._motion

    @motion.setter
    def motion(self, value):
        if not isinstance(value, bool):
            raise TypeError()
        self._timestamp = datetime.now().strftime(config.DATETIME_FORMAT)
        self._motion = value

def main():
    """main function"""
    test = Motion()
    test.id = 0
    test.motion = True
    test.room = "1"
    test.building = "Heilbronn"
    data = json.dumps(test.__dict__)
    print(data)

if __name__ == "__main__":
    main()
