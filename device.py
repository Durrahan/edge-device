# pylint: disable=C0103
"""ToDo."""

from datetime import datetime
import json
import config

class Device(object):
    """ToDo."""

    def __init__(self):
        self._eventtype = config.EVENTTYPE
        self._id = None
        self._room = None
        self._building = None
        self._timestamp = None

    @property
    def id(self):
        """ToDo."""
        return self._room

    @id.setter
    def id(self, value):
        if not isinstance(value, int):
            raise TypeError()
        self._id = value

    @property
    def room(self):
        """ToDo."""
        return self._room

    @room.setter
    def room(self, value):
        if not isinstance(value, str):
            raise TypeError()
        self._room = value

    @property
    def building(self):
        """ToDo."""
        return self._building

    @building.setter
    def building(self, value):
        if not isinstance(value, str):
            raise TypeError()
        self._building = value

    @property
    def timestamp(self):
        """ToDo."""
        return self._timestamp


def main():
    """main function"""
    test = Device()
    test.id = 0
    test.building = "Filderstadt"
    test.room = "1"
    data = json.dumps(test.__dict__)
    print(data)

if __name__ == "__main__":
    main()
