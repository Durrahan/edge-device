# pylint: disable=C0103
"""ToDo."""

from datetime import datetime
import json
import config
from device import Device

class Temperature(Device):
    """ToDo."""

    def __init__(self):
        Device.__init__(self)
        self._temperature = None
        self._humidity = None

    @property
    def temperature(self):
        """ToDo."""
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        if not isinstance(value, int):
            raise TypeError()
        self._timestamp = datetime.now().strftime(config.DATETIME_FORMAT)
        self._temperature = value

    @property
    def humidity(self):
        """ToDo."""
        return self._humidity

    @humidity.setter
    def humidity(self, value):
        if not isinstance(value, int):
            raise TypeError()
        self._timestamp = datetime.now().strftime(config.DATETIME_FORMAT)
        self._humidity = value

def main():
    """main function"""
    test = Temperature()
    test.id = 0
    test.room = "4"
    test.building = "Filderstadt"
    test.temperature = 30.0
    test.humidity = 30.0
    data = json.dumps(test.__dict__)
    print(data)

if __name__ == "__main__":
    main()
