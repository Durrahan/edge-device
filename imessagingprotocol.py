"""ToDo."""

from abc import ABCMeta, abstractmethod

class IMessagingProtocol:
    """ToDo."""
    __metaclass__ = ABCMeta

    @abstractmethod
    def connect(self):
        """ToDo."""
        raise NotImplementedError

    @abstractmethod
    def disconnect(self):
        """ToDo."""
        raise NotImplementedError

    @abstractmethod
    def publish(self, message):
        """ToDo."""
        raise NotImplementedError

    @abstractmethod
    def publish_callback(self, function):
        """ToDo."""
        raise NotImplementedError

    @abstractmethod
    def message_callback(self, function):
        """ToDo."""
        raise NotImplementedError
