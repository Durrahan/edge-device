"""ToDo."""

import sys
import json
from time import sleep
import threading
import os.path
from gpiozero import MotionSensor, RGBLED
import RPi.GPIO as GPIO
import dht11
import imessagingprotocol
import pahoprotocol
import motion
import temperature
import config

class EdgeDevice(object):
    """ToDo."""
    def __init__(self, messagingprotocol):

        # initialize GPIO
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()

        if not isinstance(messagingprotocol, imessagingprotocol.IMessagingProtocol):
            raise Exception('Bad interface')
        self._messagingprotocol = messagingprotocol

        if not os.path.isfile(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME):
            with open(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME,
                      mode="w", encoding="utf-8") as outfile:
                json.dump([], outfile)

        self._motionflagone = False
        self._motiontimerone = threading.Timer(config.MOTION_ONE_INTERVAL, self._motion_one_expire)

        self._motionflagtwo = False
        self._motiontimertwo = threading.Timer(config.MOTION_TWO_INTERVAL, self._motion_two_expire)

        self._motionone = MotionSensor(config.MOTION_ONE_GPIO)
        self._motionone.when_motion = self._when_motion_one_callback
        self._motionone.when_no_motion = self._when_no_motion_one_callback

        self._motiontwo = MotionSensor(config.MOTION_TWO_GPIO)
        self._motiontwo.when_motion = self._when_motion_two_callback
        self._motiontwo.when_no_motion = self._when_no_motion_two_callback

        self._led = RGBLED(config.LED_RED_GPIO, config.LED_GREEN_GPIO, config.LED_BLUE_GPIO)
        self._led.color = config.LED_NO_MOTION

        self.temperature = dht11.DHT11(pin=config.TEMPERATURE_GPIO)

        self.lock = threading.Lock()
        self.listthread = threading.Thread(target=self._work_list, args=[self.lock])
        self.listthread.daemon = True
        self.listthread.start()

    def start_loop(self):
        """ToDo."""
        while True:
            try:
                self._messagingprotocol.connect()
                print("Hi, Im a prototype version of the sensor listener." +
                      "Please take care of me and don't close me.")
                print("\nstop with strg+c\n")
                self._interval()
            except KeyboardInterrupt:
                print('stopping...')
                self._messagingprotocol.disconnect()
                print("listening concluded")
                sys.exit(0)
            except:
                sleep(10)

    def _append(self, file, dump):
        with open(file, mode="r", encoding="utf-8") as outfile:
            feeds = json.load(outfile)
        with open(file, mode="w", encoding="utf-8") as outfile:
            feeds.append(dump)
            json.dump(feeds, outfile)

    def _when_motion_one_callback(self):
        self._led.color = config.LED_MOTION
        
        if self._motiontimerone.is_alive():
            self._motiontimerone.cancel()
            while self._motiontimerone.is_alive():
                pass
        self._motiontimerone = threading.Timer(config.MOTION_ONE_INTERVAL, self._motion_one_expire)
        self._motiontimerone.start()
        
        if self._motionflagone:   
            return 

        self._motionflagone = True
        self._on_motion(config.MOTION_ONE_ID, True)

    def _when_no_motion_one_callback(self):
        self._led.color = config.LED_NO_MOTION
        self._motionflagone = False
        if not self._motiontimerone.is_alive():
            self._on_motion(config.MOTION_ONE_ID, False)

    def _when_motion_two_callback(self):
        self._led.color = config.LED_MOTION
        
        if self._motiontimertwo.is_alive():
            self._motiontimertwo.cancel()
            while self._motiontimertwo.is_alive():
                pass
        self._motiontimertwo = threading.Timer(config.MOTION_TWO_INTERVAL, self._motion_two_expire)
        self._motiontimertwo.start()
        
        if self._motionflagtwo:   
            return 

        self._motionflagtwo = True
        self._on_motion(config.MOTION_TWO_ID, True)

    def _when_no_motion_two_callback(self):
        self._led.color = config.LED_NO_MOTION
        self._motionflagtwo = False
        if not self._motiontimertwo.is_alive():
            self._on_motion(config.MOTION_TWO_ID, False)

    def _on_motion(self, sensor, value):
        dump = motion.Motion()
        dump.id = sensor
        dump.room = config.ROOM
        dump.building = config.BUILDING
        dump.motion = value
        print(json.dumps(self._convert(dump.__dict__)) + "\n")
        with self.lock:
            self._append(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME, self._convert(dump.__dict__))
        if not self.listthread.is_alive():
            self.listthread = threading.Thread(target=self._work_list, args=[self.lock])
            self.listthread.daemon = True
            self.listthread.start()

    def _motion_one_expire(self):
        if not self._motionflagone:
            self._on_motion(config.MOTION_ONE_ID, False)

    def _motion_two_expire(self):
        if not self._motionflagtwo:
            self._on_motion(config.MOTION_TWO_ID, False)

    def _read_temperature(self):
        result = self.temperature.read()
        if result.is_valid():
            dump = temperature.Temperature()
            dump.id = config.TEMPERATURE_ID
            dump.room = config.ROOM
            dump.building = config.BUILDING
            dump.humidity = result.humidity
            dump.temperature = result.temperature
            print(json.dumps(self._convert(dump.__dict__)) + "\n")
            with self.lock:
                self._append(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME, self._convert(dump.__dict__))
            if not self.listthread.is_alive():
                self.listthread = threading.Thread(target=self._work_list, args=[self.lock])
                self.listthread.daemon = True
                self.listthread.start()

    def _remove_element(self, element):
        with open(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME,
                  mode="r", encoding="utf-8") as data_file:
            data = json.load(data_file)
        if element in data:
            data.remove(element)
        with open(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME,
                  mode="w", encoding="utf-8") as data_file:
            json.dump(data, data_file)

    def _interval(self):
        while True:
            self._read_temperature()
            sleep(config.INTERVAL)

    def _work_list(self, lock):
        while True:
            with lock:
                with open(config.UNPUBLISHED_DIRECTORY + config.UNPUBLISHED_NAME,
                          mode="r", encoding="utf-8") as data_file:
                    data = json.load(data_file)
            if not data:
                print("Publish successful.\n")
                break
            for element in data:
                if self._messagingprotocol.publish(str(element)) == 0:
                    with lock:
                        self._remove_element(element)
                else:
                    sleep(5)

    def _convert(self, obj):
        if isinstance(obj, bool):
            return str(obj).lower()
        if isinstance(obj, (list, tuple)):
            return [self._convert(item) for item in obj]
        if isinstance(obj, dict):
            return {self._convert(key):self._convert(value) for key, value in obj.items()}
        return obj

def main():
    """main function"""
    edge = EdgeDevice(pahoprotocol.PahoProtocol())
    edge.start_loop()

if __name__ == "__main__":
    main()
