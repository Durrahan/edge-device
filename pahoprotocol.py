"""ToDo."""

import ssl
from paho.mqtt import client as mqtt
import imessagingprotocol
import pahoconfig as config

class PahoProtocol(imessagingprotocol.IMessagingProtocol):
    """ToDo."""

    def __init__(self):
        self._loop = False
        self.client = mqtt.Client(client_id=config.DEVICE_ID,
                                  clean_session=0, protocol=mqtt.MQTTv311)
        self.client.on_connect = self._on_connect
        self.client.on_disconnect = self._on_disconnect
        self.client.username_pw_set(username=config.DOMAIN + "/" +
                                    config.DEVICE_ID, password=config.SAS_TOKEN)

        self.client.tls_set(ca_certs=config.CERTIFICATE, certfile=None,
                            keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
                            tls_version=ssl.PROTOCOL_TLSv1, ciphers=None)
        self.client.tls_insecure_set(False)

        #client.loop_forever()

    def _on_connect(self, client, userdata, flags, rc):
        """ToDo."""
        self.client.subscribe("devices/" + config.DEVICE_ID + "/messages/devicebound/#")
        print("Connected rc: " + str(rc))

    @staticmethod
    def _on_disconnect(client, userdata, rc):
        """ToDo."""
        print("Disconnected rc: " + str(rc))

    def connect(self):
        self.client.connect(config.DOMAIN,
                            port=config.PORT)
        self.client.loop_start()
        self._loop = True

    def disconnect(self):
        if self._loop is True:
            self.client.loop_stop(force=False)
            self.client.disconnect()

    def publish(self, message):
        return self.client.publish("devices/" + config.DEVICE_ID + "/messages/events/",
                                   message, qos=0, retain=False).rc

    def publish_callback(self, function):
        self.client.on_publish = function

    def message_callback(self, function):
        self.client.on_message = function
